/*
 * Copyright (c) 2016, Combain Mobile AB
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.combain.cpsil;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

@TargetApi(23)
public class BLEHandler_23 extends BLEHandler {

    public static final String TAG = "BLEHandler_23";

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive (Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                    start();
                }
            }

        }

    };

    public static HashMap<String,ScanResult> foundBeacons = new HashMap<>();

    Context mContext;
    BluetoothAdapter mBTAdapter;
    BluetoothLeScanner mBLEScanner;
    ScanCallback mScanCallback;

    public BLEHandler_23(Context context) {
        super(context);

        mContext = context;
        BluetoothManager btManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBTAdapter = btManager.getAdapter();
        start();
        context.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    @Override
    public void start() {
        if (mBTAdapter == null) return;
        mBLEScanner = mBTAdapter.getBluetoothLeScanner();
        ScanSettings scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_OPPORTUNISTIC)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setNumOfMatches(ScanSettings.MATCH_NUM_MAX_ADVERTISEMENT)
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE).build();
        mScanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                if (foundBeacons != null && result != null) {
                    BluetoothDevice device = result.getDevice();
                    if (device != null) {
                        foundBeacons.put(device.getAddress(), result);
                    }
                }
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                if (foundBeacons != null) {
                    for (ScanResult result : results) {
                        if (result != null) {
                            BluetoothDevice device = result.getDevice();
                            if (device != null) foundBeacons.put(device.getAddress(), result);
                        }
                    }
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                if (Settings.DEBUG) Log.v(TAG, "SCANCALLBACK ERROR: "+errorCode);
            }
        };
        if (mBLEScanner != null) {
            if (Settings.DEBUG) Log.v(TAG, "STARTED OPPORTUNISTIC SCAN");
            mBLEScanner.startScan(null, scanSettings, mScanCallback);
        }
    }

    @Override
    public String buildDataString() {
        StringBuilder sb = new StringBuilder();
        Object[] keys = foundBeacons.keySet().toArray();
        for (Object key : keys) {
            ScanResult sr = foundBeacons.get(key);
            if (sr != null) {
                long age = Math.round((SystemClock.elapsedRealtimeNanos() - sr.getTimestampNanos()) / 1E9);
                if (age > 10) {
                    foundBeacons.remove(key);
                } else {
                    if (age < 0) age = 0;
                    BluetoothDevice device = sr.getDevice();
                    if (device != null) {
                        String type;
                        switch (device.getType()) {
                            default:
                            case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                                type = "C";
                                break;
                            case BluetoothDevice.DEVICE_TYPE_DUAL:
                                type = "D";
                                break;
                            case BluetoothDevice.DEVICE_TYPE_LE:
                                type = "L";
                                break;
                            case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                                type = "U";
                                break;
                        }
                        if (sb.length() > 0) sb.append(";");
                        sb.append("B,").append(type).append(",").append(device.getAddress()).append("\"").append(device.getName()).append("\"").append(sr.getRssi()).append(",").append(age);
                        BluetoothClass bc = device.getBluetoothClass();
                        if (device.getBluetoothClass() != null) {
                            sb.append(",").append(bc.getMajorDeviceClass()).append(",").append(bc.getDeviceClass());
                        } else {
                            sb.append(",,");
                        }
                        ScanRecord scanRecord = sr.getScanRecord();
                        if (scanRecord != null) {
                            String beaconData = buildBeaconData(scanRecord.getBytes());
                            if (beaconData != null && beaconData.length() > 0)
                                sb.append(beaconData);
                        }
                    }
                }
            }
        }
        lastSubmittedBeacons = new HashMap<>(foundBeacons);
        return sb.toString();
    }

    @Override
    public void stop() {
        if (mBLEScanner != null) {
            try {
                mBLEScanner.stopScan(mScanCallback);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        mContext.unregisterReceiver(mReceiver);
    }

    public String buildBeaconData(byte[] scanRecord) {
        //System.out.println("SCANRECORD: "+new String(scanRecord));
        try {
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5) {
                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }

            if (patternFound) {
                //Convert to hex String
                byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = Utils.bytesToHex(uuidBytes);

                //Here is your UUID
                String uuid = hexString.substring(0, 8) + "-" +
                        hexString.substring(8, 12) + "-" +
                        hexString.substring(12, 16) + "-" +
                        hexString.substring(16, 20) + "-" +
                        hexString.substring(20, 32);

                //Here is your Major value
                int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);

                //Here is your Minor value
                int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);

                Byte txPower = scanRecord[startByte + 24];

                //System.out.println("UUID: " + uuid + ", major: " + major + ", minor: " + minor + ", txPower: " + txPower);
                return "," + uuid + "," + major + "," + minor + "," + "iBeacon" + "," + txPower.intValue();
            }
        } catch (Exception e) {

        }
        return "";
    }

    private Map<String,ScanResult> lastSubmittedBeacons = new HashMap<>();

    @Override
    public boolean hasMoreBLEData() {
        if (foundBeacons == null || foundBeacons.size() == 0) return false;
        if (lastSubmittedBeacons.size() == 0) {
            if (Settings.DEBUG) Log.v(TAG, "lastSubmittedBeacons is empty");
            return true;
        } else if (foundBeacons.size()/lastSubmittedBeacons.size() > 2) {
            if (Settings.DEBUG) Log.v(TAG, "beaconsFound size increased");
            return true;
        } else {
            int nbrNew = 0;
            Object[] keys = foundBeacons.keySet().toArray();
            for (Object key : keys) {
                ScanResult sr = foundBeacons.get(key);
                if (sr != null) {
                    BluetoothDevice device = sr.getDevice();
                    if (device != null && !lastSubmittedBeacons.containsKey(device.getAddress())) {
                        nbrNew++;
                        if (nbrNew > 0.5 * lastSubmittedBeacons.size()) {
                            return true;
                        }
                    }
                }
            }
            if (Settings.DEBUG) Log.v(TAG, "BLE new: "+nbrNew+" prev: "+lastSubmittedBeacons.size());
        }
        return false;
    }

}
